/**
 * Test transform-helper.
 */


// import { IbGib_V1, IbGibRel8ns_V1, Rel8n } from '../types.mjs';
// import { TransformOpts_Rel8, IbGibAddr, IbGibRel8ns } from '../../types.mjs';
// import { getIbGibAddr, getIbAndGib } from '../../helper.mjs';
// import { pretty, clone, delay, } from '@ibgib/helper-gib';
// import { ROOT, ROOT_ADDR } from '../constants.mjs';
import { fork } from './fork.mjs';
// import { mut8 } from './mut8.mjs';
// import { rel8 } from './rel8.mjs';
import { Factory_V1 as factory } from '../factory.mjs';
import { isDna } from './transform-helper.mjs';

const PRIMITIVE_IBGIBS = [
    factory.root(),
    ...factory.primitives({
        ibs: [
            'a', '7', 'tag',
            'any string/value that isnt hashed with a gib is a primitive',
            // e.g. 6 -> 6^ -> 6^gib are all equivalent ib^gib addresses,
        ]
    }),
];

await respecfully(sir, `isDna`, async () => {
    for (const src of PRIMITIVE_IBGIBS) {

        await ifWe(sir, `should return true for dna ibgibs`, async () => {
            const resFork = await fork({ src, dna: true });
            resFork.dnas?.every(x => {
                const resIsDna = isDna({ ibGib: x });
                iReckon(sir, resIsDna).toBeTrue()
            });
        });

        await ifWe(sir, `should return false for non-dna ibgibs`, async () => {
            const resFork = await fork({ src, dna: true });
            const resIsDna = isDna({ ibGib: resFork.newIbGib });
            iReckon(sir, resIsDna).toBeFalse();
        });

    }
});
