/**
 * The core of the simplicity of the ibGib protocol is that you are
 * taking two (or more) ibGibs and producing other ibGibs.
 *
 * There are three primary functions: mut8, rel8, fork
 *
 * These are actually different aspects of the single function of
 * relationship and time, either:
 *   1) Creating a 'new' timeline, or...
 *   2) Extending an existing one.
 *
 * Mut8 is intrinsic, rel8 is extrinsic, fork is a new timeline.
 * Mut8 changes a timeline, rel8 changes a timeline's link(s),
 * fork creates a new timeline.
 */
import {
    IbGibRel8ns, IbGibAddr, IbGibWithDataAndRel8ns, Gib,
} from '../types.mjs';

/**
 * Need to see if I can remove this...
 */
export declare type IbGibData_V1 = {
    [key: string]: any;
    isTjp?: boolean;
    n?: number;
    timestamp?: string;
    uuid?: string;
};

/**
 * Convenience enum to avoid spelling mistakes. (optional)
 */
export enum Rel8n {
    past = 'past',
    ancestor = 'ancestor',
    dna = 'dna',
    identity = 'identity',
    tjp = 'tjp',
}
export interface IbGibRel8ns_V1 extends IbGibRel8ns {
    [Rel8n.past]?: IbGibAddr[];
    [Rel8n.identity]?: IbGibAddr[];
    [Rel8n.ancestor]?: IbGibAddr[];
    [Rel8n.dna]?: IbGibAddr[];
    [Rel8n.tjp]?: IbGibAddr[];
}
export interface IbGib_V1<TData = IbGibData_V1, TRel8ns extends IbGibRel8ns_V1 = IbGibRel8ns_V1>
    extends IbGibWithDataAndRel8ns<TData, TRel8ns> {
}

export interface GibInfo {
    /**
     * Hash for this ibgib frame in time.
     */
    punctiliarHash?: string;
    /**
     * The gib for this ibgib's most recent tjp.
     *
     * ## notes
     *
     * ATOW, only one tjp expected really, though I've been coding
     * with the possibility of having multiple tjp's similar to
     * checkpoints.
     */
    tjpGib?: Gib;
    /**
     * If
     */
    piecesCount?: number;
    /**
     * If a delimiter is used in this gib, this is the delimiter.
     *
     * ## notes
     *
     * ATOW, the caller already knows the delimiter. But I'm thinking that
     * I may be persisting this at some point and it would be good to include.
     */
    delimiter?: string;
    /**
     * True the gib is just 'gib' (GIB constant), else falsy.
     */
    isPrimitive?: boolean;
}