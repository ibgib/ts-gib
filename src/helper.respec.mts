import { ifWe, ifWeMight, iReckon, respecfully } from '@ibgib/helper-gib/dist/respec-gib/respec-gib.mjs';
const maam = `[${import.meta.url}]`, sir = maam;

import { getIbAndGib, getIbGibAddr } from './helper.mjs';
import { Ib, IbGib } from './types.mjs';
import { ROOT, ROOT_ADDR } from './V1/constants.mjs';


await respecfully(sir, `getIbGibAddr`, async () => {
    // unsure if these would change when not in V1...(these tests are outside V1 atow)

    await ifWe(sir, `should get the right addr for ROOT`, async () => {
        let ibGib = ROOT;
        let gotten = getIbGibAddr({ ibGib });
        iReckon(sir, gotten).isGonnaBeTruthy();
        iReckon(sir, gotten).isGonnaBe(ROOT_ADDR);
    });

    await ifWe(sir, `should get the right addr for primitives`, async () => {
        let ibs: Ib[] = ['7', 'foo', 'ibgib', 'wakka doodle'];
        for (let i = 0; i < ibs.length; i++) {
            const ib = ibs[i];
            let ibGib: IbGib = { ib, gib: 'gib' };
            let gotten = getIbGibAddr({ ibGib });
            iReckon(sir, gotten).isGonnaBeTruthy();
            iReckon(sir, gotten).isGonnaBe(`${ib}^gib`);
        }
    });

});

await respecfully(sir, `getIbAndGib`, async () => {
    // unsure if these would change when not in V1...(these tests are outside V1 atow)

    await ifWe(sir, `should get the right ib & gib for ROOT, with ibGib param`, async () => {
        let ibGib = ROOT;
        let gotten = getIbAndGib({ ibGib });
        iReckon(sir, gotten).isGonnaBeTruthy();
        iReckon(sir, gotten.ib).isGonnaBe('ib');
        iReckon(sir, gotten.gib).isGonnaBe('gib');
    });

    await ifWe(sir, `should get the right ib & gib for ROOT_ADDR, with ibGibAddr param`, async () => {
        let gotten = getIbAndGib({ ibGibAddr: ROOT_ADDR });
        iReckon(sir, gotten).isGonnaBeTruthy();
        iReckon(sir, gotten.ib).isGonnaBe('ib');
        iReckon(sir, gotten.gib).isGonnaBe('gib');
    });

    await ifWe(sir, `should get the right ib & gib for primitives`, async () => {
        let ibs: Ib[] = ['7', 'foo', 'ibgib', 'wakka doodle'];
        for (let i = 0; i < ibs.length; i++) {
            const ib = ibs[i];
            let ibGib: IbGib = { ib, gib: 'gib' };
            let gotten = getIbAndGib({ ibGib });
            iReckon(sir, gotten).isGonnaBeTruthy();
            iReckon(sir, gotten.ib).isGonnaBe(ib);
            iReckon(sir, gotten.gib).isGonnaBe('gib');
        }
    });

});
